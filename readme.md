# Basic Drupal 8 Theme Based off of the ZURB Foundation Framework

This theme includes the complete CSS and JS of the [ZURB Foundation front end framework](http://foundation.zurb.com/) (version 6.2.3)
